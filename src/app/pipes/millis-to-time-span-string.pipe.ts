import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
    name: 'millisToTimeSpanString'
})
export class MillisToTimeSpanStringPipe implements PipeTransform {

    transform(value: number): string {

        const duration = moment.duration(value);
        let retval = '';

        if (value < 0) {
            retval += '- ';
        }

        if (duration.hours() !== 0) {
            retval += `${Math.abs(duration.hours())} h `;
        }

        retval += `${Math.abs(duration.minutes())} m`;
        return retval;
    }

}
