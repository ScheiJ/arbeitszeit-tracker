import { Injectable } from '@angular/core';

@Injectable()
export class UiHelperService {

    public millisToMinutes(millis: number) {
        return millis / 1000 / 60;
    }

    public millisToHours(millis: number) {
        return millis / 1000 / 60 / 60;
    }

    public minutesToMillis(minutes: number) {
        return minutes * 60 * 1000;
    }

    public hoursToMillis(hours: number) {
        return hours * 60 * 60 * 1000;
    }

}
