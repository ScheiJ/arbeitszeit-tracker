import { Injectable } from '@angular/core';

@Injectable()
export class PersistenceService {

    public saveTimeData(entries: Array<number>) {
        window.localStorage.setItem('entries', JSON.stringify(entries));
    }

    public loadTimeData(): Array<number> {
        try {
            const entries = JSON.parse(window.localStorage.getItem('entries'));
            if (entries === null) {
                return [];
            }
            return entries;
        } catch {
            return [];
        }
    }

}
