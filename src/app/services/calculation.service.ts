import { Injectable } from '@angular/core';
import { SettingsService } from './settings.service';
import { Result, lawRequirements, Period } from '../models';

@Injectable()
export class CalculationService {

    constructor(
        private _settingsService: SettingsService
    ) { }

    public calculate(entries: Array<number>, currentTime: number) {
        const result = new Result();

        this._setPeriods(entries, currentTime, result);
        this._setPresentTime(result);
        this._setMinBreakTime(result);
        this._setLeftBreakTime(result);
        this._setOvertime(result);
        this._setEarliestEndOfDay(entries, result);

        return result;
    }

    private _setPeriods(entries: Array<number>, currentTime: number, result: Result): void {
        entries = JSON.parse(JSON.stringify(entries));

        if (entries.length % 2 === 1) {
            entries.push(currentTime);
        }

        for (let i = 0; i < entries.length - 1; i++) {
            if (i % 2 === 0) {
                // Present
                result.presentPeriods.push(new Period(entries[i], entries[i + 1]));
            } else {
                // Break
                result.breakPeriods.push(new Period(entries[i], entries[i + 1]));
            }
        }
    }

    private _setPresentTime(result: Result): void {
        result.presentTime = 0;
        for (const presentPeriod of result.presentPeriods) {
            result.presentTime += presentPeriod.duration;
        }
        result.presentTimeAfterWayTime = result.presentTime - this._settingsService.settings.wayTime;
    }

    private _setMinBreakTime(result: Result): void {
        if (result.presentTimeAfterWayTime <= lawRequirements.noBreaksTill) {
            result.minBreakTime = 0;
        } else if (result.presentTimeAfterWayTime <= lawRequirements.smallBreakTill) {
            result.minBreakTime = lawRequirements.smallBreak;
        } else {
            result.minBreakTime = lawRequirements.bigBreak;
        }
    }

    private _setLeftBreakTime(result: Result): void {
        result.leftBreakTime = result.minBreakTime;
        result.breakTimeForMinBreak = 0;
        result.breakTimeExtra = 0;

        for (const breakPeriod of result.breakPeriods) {
            if (breakPeriod.duration >= lawRequirements.minBreakTimeForLaw) {
                result.breakTimeForMinBreak += breakPeriod.duration;
            } else {
                result.breakTimeExtra += breakPeriod.duration;
            }
        }

        result.leftBreakTime = result.minBreakTime - result.breakTimeForMinBreak;

        if (result.leftBreakTime < 0) {
            result.leftBreakTime = 0;
        }
    }

    private _setOvertime(result: Result): void {
        result.overtime = result.presentTime - this._settingsService.settings.wayTime - this._settingsService.settings.workTime - result.leftBreakTime;
    }

    private _setEarliestEndOfDay(entries: Array<number>, result: Result): void {

        let endOfWorkingDayMillis;

        if (result.overtime >= 0) {
            result.earliestEndOfWorkingDay = null;
            return;
        }

        // Anfang des Arbeitstags
        endOfWorkingDayMillis = entries[0];

        // Arbeitsstunden draufrechnen
        endOfWorkingDayMillis += this._settingsService.settings.workTime;

        // Wegzeit draufrechnen
        endOfWorkingDayMillis += this._settingsService.settings.wayTime;

        // mindestpause oder pause (wenn schon >= mindestpause) draufrechnen
        if (result.minBreakTime === 0) {
            endOfWorkingDayMillis += lawRequirements.smallBreak;
        } else if (result.breakTimeForMinBreak >= result.minBreakTime) {
            endOfWorkingDayMillis += result.breakTimeForMinBreak;
        } else {
            endOfWorkingDayMillis += result.minBreakTime;
        }

        // Kurze Pausen draufrechnen
        endOfWorkingDayMillis += result.breakTimeExtra;

        result.earliestEndOfWorkingDay = new Date(endOfWorkingDayMillis);
    }
}
