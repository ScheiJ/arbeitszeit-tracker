import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { interval } from 'rxjs/observable/interval';
import { map, startWith } from 'rxjs/operators';
import { Result } from '../models';
import { CalculationService } from './calculation.service';
import { PersistenceService } from './persistence.service';

@Injectable()
export class TimeService {

    private _currentTime: Observable<number>;

    public get currentTime() {
        return this._currentTime;
    }

    private _entries: BehaviorSubject<Array<number>>;

    public get entries() {
        return this._entries.asObservable();
    }

    private _results: BehaviorSubject<Result>;

    public get results() {
        return this._results.asObservable();
    }

    constructor(
        private _calculationService: CalculationService,
        private _persistenceService: PersistenceService
    ) {

        this._results = new BehaviorSubject<Result>(new Result());

        this._currentTime = interval(4000).pipe(
            startWith(0),
            map(x => {
                return this._getNormalizedTime().getTime();
            })
        );

        this._entries = new BehaviorSubject<Array<number>>(this._persistenceService.loadTimeData());

        this._currentTime.subscribe(
            currentTime => {
                const result = this._calculationService.calculate(this._entries.getValue(), currentTime);
                this._results.next(result);
            }
        );

        this._entries.subscribe(
            entries => {
                const result = this._calculationService.calculate(this._entries.getValue(), this._getNormalizedTime().getTime());
                this._results.next(result);
                this._persistenceService.saveTimeData(entries);
            }
        );

    }

    public add(hours: number, minutes: number) {
        const entryDate = this._getNormalizedTime();
        entryDate.setHours(hours);
        entryDate.setMinutes(minutes);
        this._add(entryDate.getTime());
    }

    public addNow() {
        this._add(this._getNormalizedTime().getTime());
    }

    private _add(timestamp: number) {
        let entries = Object.assign([], this._entries.getValue());
        if (entries.indexOf(timestamp) >= 0) {
            return;
        }
        entries.push(timestamp);
        entries = entries.sort((a, b) => {
            return a - b;
        });
        this._entries.next(entries);
    }

    public remove(timestamp: number) {
        const entries = Object.assign([], this._entries.getValue());
        const index = entries.indexOf(timestamp);
        if (index < 0) {
            return;
        }
        entries.splice(index, 1);
        this._entries.next(entries);
    }

    public reset() {
        this._entries.next([]);
    }

    private _getNormalizedTime() {
        const date = new Date();
        date.setFullYear(2000);
        date.setMonth(0);
        date.setDate(1);
        date.setSeconds(0);
        date.setMilliseconds(0);
        return date;
    }
}
