import { Injectable } from '@angular/core';
import { Settings } from '../models';

@Injectable()
export class SettingsService {

    public settings: Settings;
    private _settingsSave: Settings;
    public settingsActive = false;

    constructor() {
        this._loadSettings();
    }

    public saveSettings() {
        this._settingsSave = null;
        window.localStorage.setItem('settings', JSON.stringify(this.settings));
        this.settingsActive = false;
    }

    public cancelSettings() {
        this.settings = JSON.parse(JSON.stringify(this._settingsSave));
        this._settingsSave = null;
        this.settingsActive = false;
    }

    public showSettings() {
        this._settingsSave = JSON.parse(JSON.stringify(this.settings));
        this.settingsActive = true;
    }

    private _loadSettings() {
        this.settings = JSON.parse(window.localStorage.getItem('settings'));

        if (!this.settings) {

            this.settings = {
                'workTime': 28800000,
                'wayTime': 300000,
                'maxPresentTime': 37800000
            };
            this.saveSettings();
        }
    }

}
