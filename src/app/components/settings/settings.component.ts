import { Component, OnInit } from '@angular/core';
import { UiHelperService } from '../../services/ui-helper.service';
import { SettingsService } from '../../services/settings.service';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.less']
})
export class SettingsComponent {

    constructor(
        public uiHelper: UiHelperService,
        public settings: SettingsService
    ) { }

}
