import { Component } from '@angular/core';
import { TimeService } from '../../services/time.service';
import { SettingsService } from '../../services/settings.service';

@Component({
    selector: 'app-results',
    templateUrl: './results.component.html',
    styleUrls: ['./results.component.less']
})
export class ResultsComponent {

    constructor(
        public timeService: TimeService,
        public settings: SettingsService
    ) { }

}
