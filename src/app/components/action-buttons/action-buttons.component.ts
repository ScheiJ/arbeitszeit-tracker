import { Component } from '@angular/core';
import { SettingsService } from '../../services/settings.service';
import { TimeService } from '../../services/time.service';

@Component({
  selector: 'app-action-buttons',
  templateUrl: './action-buttons.component.html',
  styleUrls: ['./action-buttons.component.less']
})
export class ActionButtonsComponent {

    constructor(
        public settings: SettingsService,
        public timeService: TimeService
    ) {}

}
