import { Component } from '@angular/core';
import { TimeService } from '../../services/time.service';

@Component({
  selector: 'app-times',
  templateUrl: './times.component.html',
  styleUrls: ['./times.component.less']
})
export class TimesComponent {

    constructor(
        public timeService: TimeService
    ) {}

}
