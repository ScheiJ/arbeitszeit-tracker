import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


import { ServiceWorkerModule } from '@angular/service-worker';
import { AppComponent } from './app.component';

import { environment } from '../environments/environment';
import { MillisToTimeSpanStringPipe } from './pipes/millis-to-time-span-string.pipe';
import { MillisToTimeStringPipe } from './pipes/millis-to-time-string.pipe';
import { UiHelperService } from './services/ui-helper.service';
import { SettingsService } from './services/settings.service';
import { SettingsComponent } from './components/settings/settings.component';
import { TimeService } from './services/time.service';
import { CalculationService } from './services/calculation.service';
import { PersistenceService } from './services/persistence.service';
import { ActionButtonsComponent } from './components/action-buttons/action-buttons.component';
import { TimesComponent } from './components/times/times.component';
import { ResultsComponent } from './components/results/results.component';

@NgModule({
  declarations: [
    AppComponent,
    MillisToTimeSpanStringPipe,
    MillisToTimeStringPipe,
    SettingsComponent,
    ActionButtonsComponent,
    TimesComponent,
    ResultsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
      UiHelperService,
      SettingsService,
      TimeService,
      CalculationService,
      PersistenceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
