import { Component } from '@angular/core';
import { SettingsService } from './services/settings.service';
import { Result } from './models';
import { TimeService } from './services/time.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less']
})
export class AppComponent {

    public input = '';
    public result: Result;

    constructor(
        public settings: SettingsService,
        public timeService: TimeService
    ) {
        this.timeService.results.subscribe(
            results => {
                this.result = results;
            }
        );
     }

    public add() {
        // Haben wir genau 4 Stellen?
        if (this.input.length !== 4) {
            return;
        }

        // Haben wir nur Ziffern?
        if (!this._onlyDigits(this.input)) {
            this.input = '';
            return;
        }

        // In Stunden und Minuten umwandeln
        const inputHours = parseInt(this.input[0] + this.input[1], 10);
        const inputMinutes = parseInt(this.input[2] + this.input[3], 10);

        // Checken dass es sich um eine valide Uhrzeit handelt
        if (inputHours > 23 || inputHours < 0 || inputMinutes > 59 || inputMinutes < 0) {
            this.input = '';
            return;
        }
        this.timeService.add(inputHours, inputMinutes);
        this.input = '';
    }

    private _onlyDigits(input: string): boolean {
        for (const char of input) {
            if (isNaN(parseFloat(char))) {
                return false;
            }
        }

        return true;
    }

}

